CREATE TABLE Player
(
    username TEXT UNIQUE,
    balance  INT CHECK (balance >= 0)
);

CREATE TABLE Shop
(
    product  TEXT UNIQUE,
    in_stock INT CHECK (in_stock >= 0),
    price    INT CHECK (price >= 0)
);

CREATE TABLE Inventory
(
    id       serial PRIMARY KEY NOT NULL,
    username TEXT               NOT NULL REFERENCES Player (username),
    product  TEXT               NOT NULL REFERENCES Shop (product),
    amount   INT CHECK (amount >= 0),
    UNIQUE (username, product),
);

INSERT INTO Player (username, balance)
VALUES ('Alice', 100);
INSERT INTO PLayer (username, balance)
VALUES ('Bob', 200);

INSERT INTO Shop (product, in_stock, price)
VALUES ('marshmello', 10, 10);

INSERT INTO Inventory (username, product, amount)
VALUES ('Bob', 'marshmello', 0);
INSERT INTO Inventory (username, product, amount)
VALUES ('Alice', 'marshmello', 0);

